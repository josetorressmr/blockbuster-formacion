Insert into COMPANY (ID, CIF, COMPANY_NOMBRE) values (1, 'US_TX_1991', 'id Software');
Insert into COMPANY (ID, CIF, COMPANY_NOMBRE) values (2, 'US_WA_1996', 'Valve Corporation');
Insert into COMPANY (ID, CIF, COMPANY_NOMBRE) values (3, 'US_CA_2002', 'Turtle Rock Studios');
Insert into COMPANY (ID, CIF, COMPANY_NOMBRE) values (4, 'US_LA_2006', 'Riot Games, Inc.');

Insert into JUEGO_COMPANIES (COMPANIES_ID, JUEGOS_ID) values (1, 1);
Insert into JUEGO_COMPANIES (COMPANIES_ID, JUEGOS_ID) values (2, 2);
Insert into JUEGO_COMPANIES (COMPANIES_ID, JUEGOS_ID) values (2, 3);
Insert into JUEGO_COMPANIES (COMPANIES_ID, JUEGOS_ID) values (3, 3);
Insert into JUEGO_COMPANIES (COMPANIES_ID, JUEGOS_ID) values (4, 4);

Insert into CLIENTE (ID, CLIENTE_USERNAME, CLIENTE_PASSWORD) values (1, 'tino', '$2y$12$5uEeq2MuOK1Rk2haEjZOU.VwunRZpQTm8drCFtRAH7..kQKHmttfi');